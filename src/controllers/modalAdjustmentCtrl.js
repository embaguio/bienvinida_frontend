class ModalAdjustmentCtrl {
    static injections() {
        return ['$uibModalInstance', '$ngConfirm', '$timeout', 'props', 'stockTransactionService', 'mainService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        console.log(this.props);
        this.form = {
            system_count : parseInt(this.props.data.item_inventory_qty),
            id_item_inventory : this.props.data.id_item_inventory
        };
    }

    add(data) {
        const modified_by = this.mainService.userInfo.id;
        this.stockTransactionService.adjustment({ ...data, modified_by })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Stock adjusted successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalAdjustmentCtrl', ModalAdjustmentCtrl.ngConstruct());
