class ModalInventoryCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'inventoryService', 'mainService', 'modalService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getCategories();
        this.getMeasurements();
        if (this.props && this.props.data) {
            this.itemEditData = this.props.data;
            this.itemEditData.item_inventory_actual_measurement = parseFloat(this.itemEditData.item_inventory_actual_measurement);
            this.itemEditData.is_consumable = !!parseInt(this.itemEditData.is_consumable);
        }
    }

    getCategories() {
        this.inventoryService.getCategories()
            .then((response) => {
                if(!response || !response.success) return;
                    this.inventory_categories = response.data;
            });
    }

    getMeasurements() {
        this.inventoryService.getMeasurements()
            .then((response) => {
                if(!response || !response.success) return;
                    this.inventory_measurements = response.data;
            });
    }

    addItem(data) {
        const modifiedBy = this.mainService.userInfo.id;
        let { sku } = data;
        let { is_consumable } = data;
        is_consumable = is_consumable ? '1' : '0';
        sku = sku || 'N/A';
        
        this.inventoryService.addItem({ ...data, modifiedBy, is_consumable, item_price: data.item_price || null, sku })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Item added successfully!', 800);
                this.close();
            });
    }

    editItem(data) {
        const modifiedBy = this.mainService.userInfo.id;
        
        this.inventoryService.editItem({ ...data, modifiedBy })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Item updated successfully!', 800);
                this.close();
            });
    }

    openCategory() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_measurement.html',
            controller: 'ModalMeasurementCtrl',
            dataForComponent: { title: 'Add Measurement Category' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getMeasurements();
          }, () => {
            console.log('cancelled');
          });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalInventoryCtrl', ModalInventoryCtrl.ngConstruct());
