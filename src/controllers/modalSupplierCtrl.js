class ModalSupplierCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'supplierService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        if (this.props && this.props.data) {
            this.supplierEditData = this.props.data;
        }
    }

    addSupplier(data) {
        this.supplierService.addSupplier(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Supplier added successfully!', 800);
                this.close();
            });
    }

    editSupplier(data) {
        this.supplierService.updateSupplier(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Supplier updated successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalSupplierCtrl', ModalSupplierCtrl.ngConstruct());
