class SupplierCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', 'toaster', 'modalService', 'supplierService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getSuppliers();
    }

    getSuppliers() {
        this.supplierService.getSuppliers()
            .then((response) => {
                if(!response) return;
                    this.suppliers = response.data || [];
            });
    }

    addSupplier() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_supplier.html',
            controller: 'ModalSupplierCtrl',
            dataForComponent: { title: 'Add Supplier' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getSuppliers();
          }, () => {
            console.log('cancelled');
          });
    }

    deleteSupplier(supplier_id) {
        this.$ngConfirm({
            title: 'Delete this supplier?',
            buttons: {
                deleteUser: {
                    text: 'Yes, delete',
                    btnClass: 'btn-red',
                    action: () => {
                        this.supplierService.deleteSupplier({ supplier_id })
                        .then((response) => {
                            if(!response || !response.success) return;
                            this.toaster.pop('success', 'Success', 'Supplier deleted successfully!', 800);
                            this.getSuppliers();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    editSupplier(data) {
        const modalSettings = {
            templateUrl: './src/views/modals/edit_supplier.html',
            controller: 'ModalSupplierCtrl',
            dataForComponent: { title: 'Edit Supplier', data },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getSuppliers();
          }, () => {
            console.log('cancelled');
          });
    }
}

app.controller('SupplierCtrl', SupplierCtrl.ngConstruct());
