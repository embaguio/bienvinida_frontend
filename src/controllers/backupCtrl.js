class BackupCtrl {
    static injections() {
        return ['$http', '$window', 'reportService', 'mainService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.get();
    }

    get() {
        this.reportService.getBackup()
            .then((response) => {
                if(!response) return;
                    this.backups = response.data || [];
                    console.log(this.backups);
            });
    }

    add() {
        this.reportService.addBackup( { user: this.mainService.userInfo.id } )
            .then((response) => {
                this.toaster.pop('success', 'Success', 'Backup successful!', 800);
                this.get();
            });
    }
}

app.controller('BackupCtrl', BackupCtrl.ngConstruct());
