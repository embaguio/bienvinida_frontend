class CustomerCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', 'toaster', 'modalService', 'customerService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getCustomers();
    }

    getCustomers() {
        this.customerService.getCustomers()
            .then((response) => {
                if(!response) return;
                    this.customers = response.data || [];
            });
    }

    addCustomer() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_customer.html',
            controller: 'ModalCustomerCtrl',
            dataForComponent: { title: 'Add Customer' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getCustomers();
          }, () => {
            console.log('cancelled');
          });
    }

    deleteCustomer(customer_id) {
        this.$ngConfirm({
            title: 'Delete this customer?',
            buttons: {
                deleteUser: {
                    text: 'Yes, delete',
                    btnClass: 'btn-red',
                    action: () => {
                        this.customerService.deleteCustomer({ customer_id })
                        .then((response) => {
                            if(!response || !response.success) return;
                            this.toaster.pop('success', 'Success', 'Customer deleted successfully!', 800);
                            this.getCustomers();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    editCustomer(data) {
        const modalSettings = {
            templateUrl: './src/views/modals/edit_customer.html',
            controller: 'ModalCustomerCtrl',
            dataForComponent: { title: 'Edit Customer', data },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getCustomers();
          }, () => {
            console.log('cancelled');
          });
    }
}

app.controller('CustomerCtrl', CustomerCtrl.ngConstruct());
