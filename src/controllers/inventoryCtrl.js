class InventoryCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', '$window', 'toaster', 'modalService', 'mainService', 'inventoryService', 'stockTransactionService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el => el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el, index) => this._defineKey(el, args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], { enumerable: true, value });
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getItems();
        this.getAdjustments();
        this.getStockTransaction();
        this.getCategories();
        this.getStockTransactionTypes();
        this.propertyName = 'age';
        this.reverse = true;
    }

    sortBy(propertyName) {
        console.log(this.inventory_items);
        this.reverse = (this.propertyName === propertyName) ? !this.reverse : false;
        this.propertyName = propertyName;
    };

    getStockTransactionTypes() {
        this.stock_transaction_types = [
            { transaction_type_id: '1', transaction_type_description: 'Stock-in' },
            { transaction_type_id: '2', transaction_type_description: 'Stock-out' },
        ];
    }

    getStockTransaction() {
        this.stockTransactionService.get()
            .then((response) => {
                if (!response || !response.success) return;
                this.stockTransactions = response.data;
            });
    }

    getAdjustments() {
        this.stockTransactionService.getAdjustments()
            .then((response) => {
                if (!response || !response.success) return;
                this.stockAdjustments = response.data;
            });
    }

    getItems() {
        this.inventoryService.getItems()
            .then((response) => {
                if (!response || !response.success) return;
                this.inventory_items = response.data;
                this.inventory_items = this.inventory_items.map((item) => {
                    item = { ...item, item_inventory_qty: parseInt(item.item_inventory_qty, 10) };
                    return item;
                });
            });
    }

    getCategories() {
        this.inventoryService.getCategories()
            .then((response) => {
                if (!response || !response.success) return;
                this.inventory_categories = response.data;
            });
    }

    addItem() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_item_inventory.html',
            controller: 'ModalInventoryCtrl',
            dataForComponent: { title: 'Add Item' },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getItems();
            }, () => {
                console.log('cancelled');
            });
    }

    editItem(data) {
        const modalSettings = {
            templateUrl: './src/views/modals/edit_item_inventory.html',
            controller: 'ModalInventoryCtrl',
            dataForComponent: { title: 'Edit Item', data: angular.copy(data) },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getItems();
            }, () => {
                console.log('cancelled');
            });
    }

    adjustment(data) {
        const modalSettings = {
            templateUrl: './src/views/modals/stock_adjust.html',
            controller: 'ModalAdjustmentCtrl',
            dataForComponent: { title: 'Stock Adjustment', data: angular.copy(data) },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getItems();
                this.getAdjustments();
            }, () => {
                console.log('cancelled');
            });
    }

    updateItemStatus(data) {
        const is_active = data.is_active === '1' ? '0' : '1';
        const { id_item_inventory } = data;
        const params = { is_active, id_item_inventory };
        const isActive = is_active === '1' ? true : false;

        if (isActive) {
            this.inventoryService.updateItemStatus(params)
                .then((response) => {
                    if (!response || !response.success) return;
                    this.toaster.pop('success', 'Success', 'Item activated successfully!', 800);
                    this.getItems();
                });
            return;
        }

        this.$ngConfirm({
            title: 'Deactivate this item?',
            content: 'Note: You can re-activate this item later on.',
            buttons: {
                deleteUser: {
                    text: 'Yes, deactivate',
                    btnClass: 'btn-red',
                    action: () => {
                        this.inventoryService.updateItemStatus(params)
                            .then((response) => {
                                if (!response || !response.success) return;
                                this.toaster.pop('success', 'Success', 'Item deactivated successfully!', 800);
                                this.getItems();
                            });
                    }
                },
                cancel: function () {
                    //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    addStockTransaction(type) {
        let title = "";

        switch (type) {
            case 1:
                title = "Stock-in Transaction";
                break;

            case 2:
                title = "Stock-out Transaction";
                break;

            default:
                title = "ERROR TRANSACTION. PLEASE REFRESH PAGE.";
                break;
        }

        const modalSettings = {
            templateUrl: "./src/views/modals/stock_transaction.html",
            controller: "ModalStockTransactionCtrl",
            dataForComponent: { title, type },
            size: "lg"
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getItems();
                this.getStockTransaction();
            }, () => {
                console.log("cancelled");
            });
    }

    printStockTransaction(data) {
        this.stockTransactionService.print(data).then(response => {
            this.$window.open(response.data, "_blank")
        });
    }
}

app.controller('InventoryCtrl', InventoryCtrl.ngConstruct());
