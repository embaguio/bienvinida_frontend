class ModalMeasurementCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'extraService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
    }


    add(data) {
        this.extraService.addMeasurement(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Added successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalMeasurementCtrl', ModalMeasurementCtrl.ngConstruct());
