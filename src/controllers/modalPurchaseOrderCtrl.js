class ModalPurchaseOrderCtrl {
    static injections() {
        return ['$uibModalInstance', '$ngConfirm', '$timeout','props', 'purchaseOrderService', 'inventoryService', 'mainService', 'supplierService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getItems();
        this.getSuppliers();
        this.purchaseOrderLines = [];
            
        this.isEdit = this.props && this.props.data;

        if (this.isEdit) {
            this.itemEditData = this.props.data;
            this.itemEditData = { ...this.itemEditData, purchase_order_posting: new Date(this.itemEditData.purchase_order_posting) };
            this.itemEditData = { ...this.itemEditData, po_due_date: new Date(this.itemEditData.po_due_date) };
            console.log(this.itemEditData);
            if(!this.itemEditData.purchase_order_lines) {
                this.itemEditData.purchase_order_lines = [{
                    id : this.purchaseOrderLines.length
                }];
                return;
            }

            this.itemEditData.purchase_order_lines.forEach((line) => {
                line.expected_qty = parseInt(line.expected_qty);
                line.received_qty = parseInt(line.received_qty);
                line.po_total_price = parseFloat(line.po_total_price);
                line.po_unit_price = parseFloat(line.po_unit_price);
            });

        } else {
            this.purchase_order = { purchase_order_posting : new Date() };
        }
    }
    
    getItems() {
        this.inventoryService.getItems()
            .then((response) => {
                if(!response || !response.success) return;
                    this.inventory_items = response.data;
            });
    }
        
    getSuppliers() {
        this.supplierService.getSuppliers()
            .then((response) => {
                if(!response) return;
                    this.suppliers = response.data || [];
                    console.log(this.suppliers);
            });
    }
        
    closeSearchItem(item) {
        console.log(item);
        this.$timeout(() => {
            item.hasSelectedItem = true;
            console.log(item);
        }, 200);
    }

    searchItemData(data){
      return (item) => {
        if (data === undefined) return true;
          if ((item.name.toLowerCase().indexOf(data.toLowerCase())!=-1 || item.sku.indexOf(data)!=-1) && item.is_active === '1' && item.id_inventory_category === '1') {
            return true;
        }
        return false;
      }
    };

    selectItem(item, po_line) {
        po_line.selectedItem = angular.copy(item);
        po_line.id_item_inventory = po_line.selectedItem.id_item_inventory;
        po_line.hasSelectedItem= true;
    }

    
    addNew(){
        if (this.isEdit) {
            this.itemEditData.purchase_order_lines.push({
                id : this.purchaseOrderLines.length
            });
        } else {
            this.purchaseOrderLines.push({
                id : this.purchaseOrderLines.length
            });
        }
    }

    remove(index){
        if (this.isEdit) {
            this.$ngConfirm({
                title: "Remove this item?",
                content: "Note: This cannot be undone.",
                buttons: {
                    deleteUser: {
                        text: "Yes, remove it",
                        btnClass: "btn-red",
                        action: () => {
                            this.purchaseOrderService
                                .deleteLine(this.itemEditData.purchase_order_lines[index].purchase_order_line_id)
                                .then(response => {
                                    if (!response || !response.success) return;
                                    this.toaster.pop("success", "Success", "Item removed successfully!", 800);
                                    this.removingPOItem = true;
                                    this.itemEditData.purchase_order_lines.splice(index, 1); 
                                    this.savePO();
                                });
                        }
                    },
                    cancel: function () {
                        //    $ngConfirm('action is canceled');
                    }
                }
            });
        } else {
            this.purchaseOrderLines.splice(index, 1); 
        }
    }

    
    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    addPO(purchaseOrder, purchaseOrderLines) {
        const modified_by = this.mainService.userInfo.id;
        const purchase_order_posting = this.getStringDate(purchaseOrder.purchase_order_posting);
        const po_due_date = this.getStringDate(purchaseOrder.po_due_date);
        
        this.purchaseOrderService.add({ ...purchaseOrder, purchase_order_posting, po_due_date, modified_by })
            .then((response) => {
                if(!response || !response.success) return;
                this.addPOLines({...purchaseOrderLines, purchase_order_id: response.data});
            });
    }

    savePO(data = this.itemEditData) {
        const modified_by = this.mainService.userInfo.id;
        const purchase_order_posting = this.getStringDate(data.purchase_order_posting);
        const po_due_date = this.getStringDate(data.po_due_date);
        
        console.log(data);

        this.purchaseOrderService.updatePO({ ...data, purchase_order_posting, po_due_date, modified_by })
            .then((response) => {
                if(!response || !response.success) return;
                this.savePOLines({...this.itemEditData.purchase_order_lines});
            });
    }

    addPOLines(purchaseOrderLines) {
        this.purchaseOrderService.addPOLines(purchaseOrderLines)
            .then((response) => {
                if(!response || !response.success) return;
                if (!this.removingPOItem) {
                    this.removingPOItem = false;
                    this.toaster.pop('success', 'Success', 'Purchase order created successfully!', 800);
                    this.close();
                }
            });

    }

    savePOLines(purchaseOrderLines) {
        console.log(purchaseOrderLines);

        this.purchaseOrderService.savePOLines(purchaseOrderLines)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Purchase order updated successfully!', 800);
                this.close();
            });

    }
    getGrandTotal() {
        this.grandTotal = 0;

        if (this.isEdit) {
            this.itemEditData && this.itemEditData.purchase_order_lines.forEach((po)=>{
                this.grandTotal+=po.po_total_price || 0;
            });
        } else {
            this.purchaseOrderLines && this.purchaseOrderLines.forEach((po)=>{
                this.grandTotal+=po.po_total_price || 0;
            });
        }

        return this.grandTotal || 0;
    }

    addItem(data) {
        const modifiedBy = this.mainService.userInfo.id;
        this.inventoryService.addItem({ ...data, modifiedBy })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Item added successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalPurchaseOrderCtrl', ModalPurchaseOrderCtrl.ngConstruct());
