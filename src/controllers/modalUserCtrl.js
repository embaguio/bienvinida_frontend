class ModalUserCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'userService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getUserRoles();
        if (this.props && this.props.data) {
            this.userEditData = this.props.data;
            this.userEditData = { ...this.userEditData, birth_date: new Date(this.userEditData.birth_date) };
        }
    }

    getUserRoles() {
        this.userService.getUserRoles()
            .then((response) => {
                if(!response || !response.success) return;
                    this.userRoles = response.data;
            });
    }

    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    addUser(data) {
        const birth_date = this.getStringDate(data.birth_date);
        this.userService.addUser( { ...data, birth_date })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User added successfully!', 800);
                this.close();
            });
    }

    editUser(data) {
        const birth_date = this.getStringDate(data.birth_date);
        this.userService.updateUser({ ...data, birth_date })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User updated successfully!', 800);
                this.close();
            });
    }

    checkPassword(p1, p2) {
        this.isInvalidPassword = !(p1 === p2);
    }

    resetUserPassword(user, data) {

        this.userService.updatePassword({ ...data, id: user.id })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User password updated successfully!', 800);
                this.close();
            });
    }


    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalUserCtrl', ModalUserCtrl.ngConstruct());
