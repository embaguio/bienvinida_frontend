class DashboardCtrl {
    static injections() {
        return ['$http', 'dashboardService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.get();
    }

    get() {
        this.dashboardService.get()
            .then((response) => {
                if(!response) return;
                    this.dashboard = response || [];
            });
    }

}

app.controller('DashboardCtrl', DashboardCtrl.ngConstruct());
