class ModalExpensesCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'expensesService', 'mainService', 'modalService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getCategories();
        this.form = { expense_posting : new Date };

        if (this.props && this.props.data) {
            this.expenseEditData = this.props.data;
        }
    }
    
    addExpenseCategory() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_expense_category.html',
            controller: 'ModalExpenseCategorytCtrl',
            dataForComponent: { title: 'Add Expense Category' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getCategories();
          }, () => {
            console.log('cancelled');
          });
    }

    getCategories() {
        this.expensesService.getCategories()
            .then((response) => {
                if(!response || !response.success) return;
                    this.expense_types = response.data;
            });
    }

    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    addExpense(data) {
        const modified_by = this.mainService.userInfo.id;
        const expense_posting = this.getStringDate(data.expense_posting);

        data = {...data, modified_by, expense_posting };
        this.expensesService.addExpense(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Expense added successfully!', 800);
                this.close();
            });
    }

    editExpense(data) {
        const modified_by = this.mainService.userInfo.id;
        data = {...data, modified_by : modified_by};
        this.expensesService.updateExpense(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Expense updated successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalExpensesCtrl', ModalExpensesCtrl.ngConstruct());
