class PurchaseOrderCtrl {
    static injections() {
        return [
            "$scope",
            "$ngConfirm",
            "$window",
            "toaster",
            "modalService",
            "purchaseOrderService"
        ];
    }
    static ngConstruct() {
        return [...this.injections().map(el => el.split(":").pop()), this];
    }
    constructor(...args) {
        this.constructor
            .injections()
            .forEach((el, index) => this._defineKey(el, args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(":")[0], { enumerable: true, value });
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getPOs();
    }

    getPOs() {
        this.purchaseOrderService.get().then(response => {
            if (!response) return;
            this.purchase_orders = response.data || [];
        });
    }

    addPO() {
        const modalSettings = {
            templateUrl: "./src/views/modals/add_po.html",
            controller: "ModalPurchaseOrderCtrl",
            dataForComponent: { title: "Create New Purchase Order" },
            size: "lg"
        };

        this.modalService.open(modalSettings).result.then(
            () => {
                this.getPOs();
            },
            () => {
                console.log("cancelled");
            }
        );
    }

    getLines(purchase_order_id) {
        return this.purchaseOrderService.getLines({ purchase_order_id }).then(response => {
            if (!response || !response.success) return;
            this.purchase_order_lines = response.data;
        });
    }

    editItem(data) {
        this.getLines(data.purchase_order_id)
        .then(()=>{
            const modalSettings = {
                    templateUrl: "./src/views/modals/edit_po.html",
                    controller: "ModalPurchaseOrderCtrl",
                    dataForComponent: { title: "Edit Purchase Order", data: { ... data, purchase_order_lines: this.purchase_order_lines } }
                };
        
                this.modalService.open(modalSettings).result.then(
                    () => {
                        this.getPOs();
                    },
                    () => {
                        console.log("cancelled");
                    }
                );
        });
    }

    print(data) {
        this.purchaseOrderService.print(data).then(response => {
           this.$window.open(response.data, "_blank")
        });
    }

    updateStatus(data) {
        const is_active = data.is_active === "1" ? "0" : "1";
        const { purchase_order_id } = data;
        const params = { is_active, purchase_order_id };
        const isActive = is_active === "1" ? true : false;

        if (isActive) {
            this.purchaseOrderService.updateStatus(params).then(response => {
                if (!response || !response.success) return;
                this.toaster.pop(
                    "success",
                    "Success",
                    "Item activated successfully!",
                    800
                );
                this.getPOs();
            });
            return;
        }

        this.$ngConfirm({
            title: "Deactivate this item?",
            content: "Note: You can re-activate this item later on.",
            buttons: {
                deleteUser: {
                    text: "Yes, deactivate",
                    btnClass: "btn-red",
                    action: () => {
                        this.purchaseOrderService
                            .updateStatus(params)
                            .then(response => {
                                if (!response || !response.success) return;
                                this.toaster.pop(
                                    "success",
                                    "Success",
                                    "Item deactivated successfully!",
                                    800
                                );
                                this.getPOs();
                            });
                    }
                },
                cancel: function () {
                    //    $ngConfirm('action is canceled');
                }
            }
        });
    }
}

app.controller("PurchaseOrderCtrl", PurchaseOrderCtrl.ngConstruct());
