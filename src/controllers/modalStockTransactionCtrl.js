class ModalStockTransactionCtrl {
    static injections() {
        return ['$uibModalInstance', '$ngConfirm', '$timeout', 'props', 'stockTransactionService', 'mainService', 'inventoryService', 'supplierService', 'purchaseOrderService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getItems();
        this.getPOs();
        this.getSuppliers();
        this.stockTransactionLines = [];
        this.form = { stock_transaction_total : 0 };  
        this.form = { stock_transaction_date : new Date() };

        this.isEdit = this.props && this.props.data;

        if (this.isEdit) {
            this.itemEditData = this.props.data;
            this.itemEditData = { ...this.itemEditData, stock_transaction_date: new Date(this.itemEditData.stock_transaction_date) };
        }
    }


    getSuppliers() {
        this.supplierService.getSuppliers()
            .then((response) => {
                if(!response) return;
                    this.suppliers = response.data || [];
                    console.log(this.suppliers);
            });
    }

    getItems() {
        this.inventoryService.getItems()
            .then((response) => {
                if(!response || !response.success) return;
                    this.inventory_items = response.data;
            });
    }

    getPOs() {
        this.purchaseOrderService.get().then(response => {
            if (!response) return;
            this.purchase_orders = response.data || [];
        });
    }

    getPOLines(purchase_order_id) {
        console.log(this.selectedPO);
        this.form.supplier_id = this.selectedPO.supplier_id;
        this.form.stock_transaction_description = this.selectedPO.stock_transaction_description;
        this.isFromPO = true;

        this.form = { ...this.form, id_inventory_category : '1' };

        return this.purchaseOrderService.getLines({ purchase_order_id }).then(response => {
            if (!response) return;
            this.purchase_order_lines = response.data;
            this.setTransactionLines(this.purchase_order_lines);
        });
    }

    checkStockAvailability() {
        let error = false;
        this.stockTransactionLines.forEach( (line) => {
            if (line.selectedItem && !line.selectedItem.isAvailableStock && line.selectedItem.isAvailableStock != undefined && line.selectedItem.isAvailableStock != null ) {
                error = true;
            }
        });
        console.log(error);
        return error;
    }

    setTransactionLines(data) {
        this.stockTransactionLines = data.map((line) => {
            return {
                hasSelectedItem: true,
                id_item_inventory: line.id_item_inventory,
                selectedItem: { id_item_inventory: line.id_item_inventory, sku: line.sku, name: line.name,item_inventory_qty: line.item_inventory_qty },
                stock_transaction_line_qty: parseInt(line.expected_qty)
            }
        });
    }

    calculateTotal() {
        let total = 0;

        if (this.isEdit) {
            this.itemEditData && this.itemEditData.stock_in_lines.forEach((po)=>{
                total+=parseFloat(line.stock_transaction_line_qty) || 0;
            });
        } else {
            this.stockTransactionLines && this.stockTransactionLines.forEach((line)=>{
                total+=parseFloat(line.stock_transaction_line_qty) || 0;
            });
        }
        return total || 0;
    }
    
    closeSearchItem(item) {
        this.$timeout(() => {
            item.hasSelectedItem = true;
        }, 200);
    }
    
    closeSearchItem(item) {
        this.$timeout(() => {
            item.hasSelected = true;
            console.log(item);
        }, 200);
    }

    searchItemData(data){
      return (item) => {
        if (data === undefined) return true;
          if ((item.name.toLowerCase().indexOf(data.toLowerCase())!=-1 || item.sku.indexOf(data)!=-1) && item.is_active === '1') {
            return true;
        }
        return false;
      }
    };

    searchPOData(data){
      return (item) => {
        if (data === undefined) return true;
          if ((item.purchase_order_description.toLowerCase().indexOf(data.toLowerCase())!=-1 || item.purchase_order_id.indexOf(data)!=-1) && item.is_active === '1') {
            return true;
        }
        return false;
      }
    };

    selectItem(item, stockTransactionLine) {
        stockTransactionLine.selectedItem = angular.copy(item);
        stockTransactionLine.id_item_inventory = stockTransactionLine.selectedItem.id_item_inventory;
        stockTransactionLine.hasSelectedItem = true;
    }

    selectPO(item, po) {
        po.supplier_id = item.supplier_id;
        po.stock_transaction_description = item.purchase_order_description;
        po.purchase_order_id = item.purchase_order_id;
        po.hasSelected = true;
    }
    
    addNew(){
        if (this.isEdit) {
            this.itemEditData.stock_in_lines.push({
                id : this.stockTransactionLines.length
            });
        } else {
            this.stockTransactionLines.push({
                id : this.stockTransactionLines.length
            });
        }
    }

    remove(index){
        this.stockTransactionLines.splice(index, 1); 
    }

    
    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    createStockTransaction(stockTransactionData, stockTransactionLines) {
        const modified_by = this.mainService.userInfo.id;
        const stock_transaction_date = this.getStringDate(stockTransactionData.stock_transaction_date);
        const transaction_type_id = this.props.type;
        const supplier_id = stockTransactionData.supplier_id || null;
        
        this.stockTransactionService.add({ ...stockTransactionData, stock_transaction_date, modified_by, transaction_type_id, supplier_id })
            .then((response) => {
                if(!response || !response.success) return;
                console.log(response);
                this.addLines({...stockTransactionLines, stock_transaction_id: response.data, transaction_type_id });
            });
    }

    addLines(stockTransactionLines) {
        console.log(stockTransactionLines);
        this.stockTransactionService.addLines(stockTransactionLines)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Stock transaction was successful!', 800);
                this.close();
                
            });

    }

    addItem(data) {
        const modifiedBy = this.mainService.userInfo.id;
        this.inventoryService.addItem({ ...data, modifiedBy })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Item added successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalStockTransactionCtrl', ModalStockTransactionCtrl.ngConstruct());
