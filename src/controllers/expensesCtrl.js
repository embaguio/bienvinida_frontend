class ExpensesCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', 'toaster', 'modalService', 'expensesService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getExpenses();
    }

    getExpenses() {
        this.expensesService.getExpenses()
            .then((response) => {
                if(!response) return;
                    this.expenses = response.data || [];
            });
    }

    addExpense() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_expense.html',
            controller: 'ModalExpensesCtrl',
            dataForComponent: { title: 'Add Expense' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.getExpenses();
          }, () => {
            console.log('cancelled');
          });
    }

    deleteExpense(expense_id) {
        this.$ngConfirm({
            title: 'Delete this expense?',
            buttons: {
                deleteUser: {
                    text: 'Yes, delete',
                    btnClass: 'btn-red',
                    action: () => {
                        this.expensesService.delete({ expense_id })
                        .then((response) => {
                            if(!response || !response.success) return;
                            this.toaster.pop('success', 'Success', 'Expense deleted successfully!', 800);
                            this.getExpenses();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }
}

app.controller('ExpensesCtrl', ExpensesCtrl.ngConstruct());
