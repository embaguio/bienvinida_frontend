class SalesCtrl {
    static injections() {
        return ['$window', '$ngConfirm', 'modalService', 'salesService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.get();
    }

    get() {
        this.salesService.get().then(response => {
            if (!response) return;
            this.sales = response.data || [];
            console.log(this.sales);
        });
    }

    addSales() {
        const modalSettings = {
            templateUrl: "./src/views/modals/add_sales.html",
            controller: "ModalSalesCtrl",
            dataForComponent: { title: "Add New Sales" },
            size: "lg"
        };

        this.modalService.open(modalSettings).result.then(
            () => {
                this.get();
            },
            () => {
                console.log("cancelled");
            }
        );
    }

    cancel(data) {
        this.$ngConfirm({
            title: 'Cancel this order?',
            content: `Note: You can't undo this later on.`,
            buttons: {
                deleteUser: {
                    text: 'Yes',
                    btnClass: 'btn-red',
                    action: () => {
                        this.salesService.cancel(data).then((response) => {
                            if (!response.success) return; 
                            this.toaster.pop('success', 'Success', 'Order cancelledd successfully!', 800);
                            this.get();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    print(data) {
        this.salesService.print(data).then(response => {
           this.$window.open(response.data, "_blank")
        });
    }
}

app.controller('SalesCtrl', SalesCtrl.ngConstruct());
