class UserManagementCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', 'toaster', 'modalService', 'userService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el => el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el, index) => this._defineKey(el, args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], { enumerable: true, value });
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getUsers();
    }

    getUsers() {
        this.userService.getUsers()
            .then((response) => {
                if (!response) return;
                this.users = response.data || [];
            });
    }

    addUser() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_user.html',
            controller: 'ModalUserCtrl',
            dataForComponent: { title: 'Add User' },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getUsers();
            }, () => {
                console.log('cancelled');
            });
    }

    deleteUser(id) {
        this.$ngConfirm({
            title: 'Delete this user?',
            buttons: {
                deleteUser: {
                    text: 'Yes, delete',
                    btnClass: 'btn-red',
                    action: () => {
                        this.userService.deleteUser({ id })
                            .then((response) => {
                                if (!response || !response.success) return;
                                this.toaster.pop('success', 'Success', 'User deleted successfully!', 800);
                                this.getUsers();
                            });
                    }
                },
                cancel: function () {
                    //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    editUser(user) {
        const modalSettings = {
            templateUrl: './src/views/modals/edit_user.html',
            controller: 'ModalUserCtrl',
            dataForComponent: { title: 'Edit User', data: user },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getUsers();
            }, () => {
                console.log('cancelled');
            });
    }

    resetPassword(user) {
        const modalSettings = {
            templateUrl: './src/views/modals/reset_user_password.html',
            controller: 'ModalUserCtrl',
            dataForComponent: { title: 'Reset User Password', data: user },
        };

        this.modalService.open(modalSettings).result
            .then(() => {
                this.getUsers();
            }, () => {
                console.log('cancelled');
            });
    }
}

app.controller('UserManagementCtrl', UserManagementCtrl.ngConstruct());
