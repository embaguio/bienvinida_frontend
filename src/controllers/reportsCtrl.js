class ReportsCtrl {
    static injections() {
        return ['$http', '$window', 'reportService', 'mainService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.form = { end : new Date() };
        this.get();
    }

    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    generateReport() {
        const { start, end, type, order, category } = this.form;
        const by = this.mainService.userInfo.id;

        let data = {};
        if ( type === 'inventory') {
            data = { type, order,category, by };
        } else {
            data = { start:this.getStringDate(start), end:this.getStringDate(end), type, by };
            if (type === 'in' || type === 'out') data = { ...data, category };
        }
        // return;

        this.reportService.generate(data).then(response => {
            this.$window.open(response.data, "_blank");
            this.get();
         });   
    }

    get() {
        this.reportService.get()
            .then((response) => {
                if(!response) return;
                    this.reports = response.data || [];
            });
    }
}

app.controller('ReportsCtrl', ReportsCtrl.ngConstruct());
