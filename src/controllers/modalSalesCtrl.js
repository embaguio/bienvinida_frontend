class ModalSalesCtrl {
    static injections() {
        return ['$uibModalInstance', '$ngConfirm', '$timeout','props', 'salesService', 'inventoryService', 'mainService', 'customerService', 'toaster'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getItems();
        this.getCustomers();
        this.salesLines = [];

        this.sales_order = { ...this.sales_order, sales_order_posting: new Date() };
            
        this.isEdit = this.props && this.props.data;

        if (this.isEdit) {
            this.itemEditData = this.props.data;
            this.itemEditData = { ...this.itemEditData, purchase_order_posting: new Date(this.itemEditData.purchase_order_posting) };
            this.itemEditData = { ...this.itemEditData, po_due_date: new Date(this.itemEditData.po_due_date) };
            console.log(this.itemEditData);
            if(!this.itemEditData.salesLines) {
                this.itemEditData.salesLines = [{
                    id : this.salesLines.length
                }];
                return;
            }

            this.itemEditData.salesLines.forEach((line) => {
                line.expected_qty = parseInt(line.expected_qty);
                line.received_qty = parseInt(line.received_qty);
                line.po_total_price = parseFloat(line.po_total_price);
                line.po_unit_price = parseFloat(line.po_unit_price);
            });

        }
    }

    checkStockAvailability() {
        let error = false;
        this.salesLines.forEach( (line) => {
            if (line.selectedItem && !line.selectedItem.isAvailableStock && line.selectedItem.isAvailableStock != undefined && line.selectedItem.isAvailableStock != null ) {
                error = true;
            }
        });
        console.log(error);
        return error;
    }
    
    getItems() {
        this.inventoryService.getItems()
            .then((response) => {
                if(!response || !response.success) return;
                    this.inventory_items = response.data;
            });
    }
    
    getCustomers() {
        this.customerService.getCustomers()
            .then((response) => {
                if(!response) return;
                    this.customers = response.data || [];
            });
    }

    setLineDiscountPrice(line) {
        const sales_order_discount_rate = (parseFloat(line.sales_order_discount_rate)/100);
        const price = parseFloat(line.selectedItem.item_price);
        console.log('discounnnnnt');
        // line.sales_order_discount_rate = angular.copy(sales_order_discount_rate);
        line.sales_line_total = (price - (price * sales_order_discount_rate ))*parseInt(line.sales_line_qty);
    }
        
    closeSearchItem(item) {
        this.$timeout(() => {
            item.hasSelectedItem = true;
        }, 200);
    }

    searchItemData(data){
      return (item) => {
        if (data === undefined) return true;
          if ((item.name.toLowerCase().indexOf(data.toLowerCase())!=-1 || item.sku.indexOf(data)!=-1) && item.is_active === '1' && item.id_inventory_category === '2') {
            return true;
        }
        return false;``
      }
    };

    selectItem(item, po_line) {
        po_line.selectedItem = angular.copy(item);
        po_line.id_item_inventory = po_line.selectedItem.id_item_inventory;
        po_line.hasSelectedItem= true;
    }

    
    addNew(){
        if (this.isEdit) {
            this.itemEditData.salesLines.push({
                id : this.salesLines.length
            });
        } else {
            this.salesLines.push({
                id : this.salesLines.length
            });
        }
    }

    remove(index){
        if (this.isEdit) {
            this.$ngConfirm({
                title: "Remove this item?",
                content: "Note: This cannot be undone.",
                buttons: {
                    deleteUser: {
                        text: "Yes, remove it",
                        btnClass: "btn-red",
                        action: () => {
                            this.salesService
                                .deleteLine(this.itemEditData.salesLines[index].purchase_order_line_id)
                                .then(response => {
                                    if (!response || !response.success) return;
                                    this.toaster.pop("success", "Success", "Item removed successfully!", 800);
                                    this.removingPOItem = true;
                                    this.itemEditData.salesLines.splice(index, 1); 
                                    this.savePO();
                                });
                        }
                    },
                    cancel: function () {
                        //    $ngConfirm('action is canceled');
                    }
                }
            });
        } else {
            this.salesLines.splice(index, 1); 
        }
    }

    
    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    addSales(order, salesLines) {
        const modified_by = this.mainService.userInfo.id;
        const sales_order_posting = this.getStringDate(order.sales_order_posting);
        
        this.salesService.add({ ...order, sales_order_posting, modified_by })
            .then((response) => {
                if(!response || !response.success) return;
                console.log(response.data);
                this.addLines({...salesLines, sales_order_id: response.data});
            });
    }

    addLines(salesLines) {
        this.salesService.addLines(salesLines)
            .then((response) => {
                if(!response || !response.success) return;

                this.toaster.pop('success', 'Success', 'Added sales successfully!', 800);
                this.close();
            });

    }

    getGrandTotal() {
        this.grandTotal = 0;

        if (this.isEdit) {
            this.itemEditData && this.itemEditData.salesLines.forEach((po)=>{
                this.grandTotal+=po.sales_line_total || 0;
            });
        } else {
            this.salesLines && this.salesLines.forEach((po)=>{
                this.grandTotal+=po.sales_line_total || 0;
            });
        }

        return this.grandTotal || 0;
    }

    addItem(data) {
        const modifiedBy = this.mainService.userInfo.id;
        this.inventoryService.addItem({ ...data, modifiedBy })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Item added successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalSalesCtrl', ModalSalesCtrl.ngConstruct());
