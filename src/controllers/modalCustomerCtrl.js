class ModalCustomerCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'customerService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        if (this.props && this.props.data) {
            this.customerEditData = this.props.data;
        }
    }

    addCustomer(data) {
        this.customerService.addCustomer(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Customer added successfully!', 800);
                this.close();
            });
    }

    editCustomer(data) {
        this.customerService.updateCustomer(data)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Customer updated successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalCustomerCtrl', ModalCustomerCtrl.ngConstruct());
