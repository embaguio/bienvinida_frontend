class StockTransactionService {
  constructor($http, mainService) {
    this.$http = $http;
    this.mainService = mainService;
    this.apiBaseUrl = this.mainService.apiBaseUrl;
  }

  get() {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/get`)
      .then(response => {
        return response && response.data;
      });
  }

  getLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/get-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  add(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/add`, data)
      .then(response => {
        return response && response.data;
      });
  }

  updatePO(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/update-po`, data)
      .then(response => {
        return response && response.data;
      });
  }

  addLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/add-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  saveLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/update-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  print(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/print`, data)
      .then(response => {
        return response && response.data;
      });
  }

  adjustment(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/adjustment`, data)
      .then(response => {
        return response && response.data;
      });
  }

  getAdjustments() {
    return this.$http
      .post(`${this.apiBaseUrl}/stock-transaction/get-adjustments`)
      .then(response => {
        return response && response.data;
      });
  }
}
app.service("stockTransactionService", StockTransactionService);
