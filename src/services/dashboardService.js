class DashboardService {

  constructor($http, mainService) {
    this.$http = $http;
    this.mainService = mainService;
    this.apiBaseUrl = this.mainService.apiBaseUrl;
  }

  get() {
    return this.$http.post(`${this.apiBaseUrl}/reports/dashboard`)
        .then((response) => {
            return response && response.data;
        });
  }  
}
app.service('dashboardService', DashboardService);