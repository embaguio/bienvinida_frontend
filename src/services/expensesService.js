class ExpensesService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    getExpenses() {
      return this.$http.post(`${this.apiBaseUrl}/expense/get`)
          .then((response) => {
              return response && response.data;
          });
    }   

    addExpense(data) {
        return this.$http.post(`${this.apiBaseUrl}/expense/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }

    delete(data) {
        return this.$http.post(`${this.apiBaseUrl}/expense/delete`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    getCategories() {
      return this.$http.post(`${this.apiBaseUrl}/expense/get-categories`)
          .then((response) => {
              return response && response.data;
          });
    }

}
app.service('expensesService', ExpensesService);