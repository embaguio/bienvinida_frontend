class CustomerService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    getCustomers() {
      return this.$http.post(`${this.apiBaseUrl}/customer/get-customers`)
          .then((response) => {
              return response && response.data;
          });
    }   
        
    getUserRoles() {
      return this.$http.post(`${this.apiBaseUrl}/customer/get-user-roles`)
          .then((response) => {
              return response && response.data;
          });
    }   

    addCustomer(data) {
        return this.$http.post(`${this.apiBaseUrl}/customer/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    updateCustomer(data) {
        return this.$http.post(`${this.apiBaseUrl}/customer/update`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    deleteCustomer(id) {
        return this.$http.post(`${this.apiBaseUrl}/customer/delete`, id)
            .then((response) => {
                return response && response.data;
            });
    }

    updatePassword(data) {
        return this.$http.post(`${this.apiBaseUrl}/customer/update-password`, data)
            .then((response) => {
                return response && response.data;
            });
    }
}
app.service('customerService', CustomerService);