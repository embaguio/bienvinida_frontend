class ReportService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    generate(data) {
        return this.$http
        .post(`${this.apiBaseUrl}/reports/${data.type}`, data)
        .then(response => {
            return response && response.data;
        });
    }
        
    get() {
      return this.$http.post(`${this.apiBaseUrl}/reports/get`)
          .then((response) => {
              return response && response.data;
          });
    }  
        
    getBackup() {
      return this.$http.post(`${this.apiBaseUrl}/reports/get-backup`)
          .then((response) => {
              return response && response.data;
          });
    }
        
    addBackup(data) {
        return this.$http
        .post(`${this.apiBaseUrl}/reports/backup`, data)
        .then(response => {
            return response && response.data;
        });
    }
}
app.service('reportService', ReportService);