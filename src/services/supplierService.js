class SupplierService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    getSuppliers() {
      return this.$http.post(`${this.apiBaseUrl}/supplier/get-suppliers`)
          .then((response) => {
              return response && response.data;
          });
    }   

    addSupplier(data) {
        return this.$http.post(`${this.apiBaseUrl}/supplier/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    updateSupplier(data) {
        return this.$http.post(`${this.apiBaseUrl}/supplier/update`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    deleteSupplier(id) {
        return this.$http.post(`${this.apiBaseUrl}/supplier/delete`, id)
            .then((response) => {
                return response && response.data;
            });
    }

}
app.service('supplierService', SupplierService);