class PurchaseOrderService {
  constructor($http, mainService) {
    this.$http = $http;
    this.mainService = mainService;
    this.apiBaseUrl = this.mainService.apiBaseUrl;
  }

  get() {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/get`)
      .then(response => {
        return response && response.data;
      });
  }

  getLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/get-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  add(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/add`, data)
      .then(response => {
        return response && response.data;
      });
  }

  updatePO(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/update-po`, data)
      .then(response => {
        return response && response.data;
      });
  }

  addPOLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/add-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  savePOLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/update-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  getCategories() {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/get-categories`)
      .then(response => {
        return response && response.data;
      });
  }

  getMeasurements() {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/get-measurements`)
      .then(response => {
        return response && response.data;
      });
  }

  addItem(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/add-item`, data)
      .then(response => {
        return response && response.data;
      });
  }

  updateStatus(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/update-status`, data)
      .then(response => {
        return response && response.data;
      });
  }

  deleteLine(purchase_order_line_id) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/delete-line`, { purchase_order_line_id })
      .then(response => {
        return response && response.data;
      });
  }

  print(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/purchase-order/print`, data)
      .then(response => {
        return response && response.data;
      });
  }
}
app.service("purchaseOrderService", PurchaseOrderService);
