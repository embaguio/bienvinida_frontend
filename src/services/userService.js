class UserService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    getUsers() {
      return this.$http.post(`${this.apiBaseUrl}/user/get-users`)
          .then((response) => {
              return response && response.data;
          });
    }   
        
    getUserRoles() {
      return this.$http.post(`${this.apiBaseUrl}/user/get-user-roles`)
          .then((response) => {
              return response && response.data;
          });
    }   

    addUser(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    updateUser(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/update`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    deleteUser(id) {
        return this.$http.post(`${this.apiBaseUrl}/user/delete`, id)
            .then((response) => {
                return response && response.data;
            });
    }

    updatePassword(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/update-password`, data)
            .then((response) => {
                return response && response.data;
            });
    }
}
app.service('userService', UserService);