class SalesService {
  constructor($http, mainService) {
    this.$http = $http;
    this.mainService = mainService;
    this.apiBaseUrl = this.mainService.apiBaseUrl;
  }

  get() {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/get`)
      .then(response => {
        return response && response.data;
      });
  }

  getLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/get-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  add(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/add`, data)
      .then(response => {
        return response && response.data;
      });
  }

  addLines(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/add-lines`, data)
      .then(response => {
        return response && response.data;
      });
  }

  addItem(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/add-item`, data)
      .then(response => {
        return response && response.data;
      });
  }

  updateStatus(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/update-status`, data)
      .then(response => {
        return response && response.data;
      });
  }


  print(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/print`, data)
      .then(response => {
        return response && response.data;
      });
  }

  cancel(data) {
    return this.$http
      .post(`${this.apiBaseUrl}/sales/cancel`, data)
      .then(response => {
        return response && response.data;
      });
  }
}
app.service("salesService", SalesService);
