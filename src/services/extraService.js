class ExtraService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    addMeasurement(data) {
        return this.$http.post(`${this.apiBaseUrl}/extra/add-measurement`, data)
            .then((response) => {
                return response && response.data;
            });
    }  

    addExpenseCategory(data) {
        return this.$http.post(`${this.apiBaseUrl}/extra/add-expense-category`, data)
            .then((response) => {
                return response && response.data;
            });
    }    
}
app.service('extraService', ExtraService);