var app = angular.module('app', ['ui.router', 'chart.js', 'ui.bootstrap', 'toaster', 'cp.ngConfirm']);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $uibModalProvider) {
    $httpProvider.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded' };
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }

    $urlRouterProvider.otherwise('/dashboard');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'src/views/login.html',
            controller: 'MainCtrl as vm',
            title: 'Login'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'src/views/dashboard.html',
            controller: 'DashboardCtrl as vm',
            title: 'Dashboard',
            accessible: ['Administrator', 'Corporate', 'Employee'],
        })
        
        .state('inventory', {
            url: '/inventory',
            templateUrl: 'src/views/inventory.html',
            controller: 'InventoryCtrl as vm',
            title: 'Inventory',
            accessible: ['Administrator', 'Corporate', 'Employee'],
        })
        
        .state('purchase-order', {
            url: '/purchase-order',
            templateUrl: 'src/views/purchase-order.html',
            controller: 'PurchaseOrderCtrl as vm',
            title: 'Purchase Order',
            accessible: ['Administrator', 'Corporate'],
        })

        .state('sales', {
            url: '/sales',
            templateUrl: 'src/views/sales.html',
            controller: 'SalesCtrl as vm',
            title: 'Sales',
            accessible: ['Administrator', 'Corporate'],
        })
        
        .state('expenses', {
            url: '/expenses',
            templateUrl: 'src/views/expenses.html',
            controller: 'ExpensesCtrl as vm',
            title: 'Expenses',
            accessible: ['Administrator', 'Corporate'],
        })
        
        .state('reports', {
            url: '/reports',
            templateUrl: 'src/views/reports.html',
            controller: 'ReportsCtrl as vm',
            title: 'Reports',
            accessible: ['Administrator'],
        })

        .state('suppliers', {
            url: '/suppliers',
            templateUrl: 'src/views/suppliers.html',
            controller: 'SupplierCtrl as vm',
            title: 'Suppliers',
            accessible: ['Administrator', 'Corporate'],
        })
        
        .state('customers', {
            url: '/customers',
            templateUrl: 'src/views/customers.html',
            controller: 'CustomerCtrl as vm',
            title: 'Customers',
            accessible: ['Administrator', 'Corporate'],
        })
        
        .state('user-management', {
            url: '/user-management',
            templateUrl: 'src/views/user-management.html',
            controller: 'UserManagementCtrl as vm',
            title: 'User Management',
            accessible: ['Administrator'],
        })
        
        .state('backup', {
            url: '/backup',
            templateUrl: 'src/views/backup.html',
            controller: 'BackupCtrl as vm',
            title: 'Backup',
            accessible: ['Administrator'],
        })

});

app.run(function ($rootScope, $location, $state, mainService) {
    $rootScope.$watch(function(){
        return $state.$current.name
    }, function(){
        const accessible = $state.$current.accessible;
        const role = mainService.getLoggedIn() && mainService.getLoggedIn().role;
        const loggedIn = mainService.isLoggedin();
        const onLoginPage = (loggedIn && $state.current.name === 'login');

        const isGranted = !(accessible && accessible.indexOf(role) === -1);

        if (!loggedIn) {
            $state.go('login');
        } else if ( !isGranted || onLoginPage ) {
            $state.go('dashboard');
        }

        
    }) 
  });
